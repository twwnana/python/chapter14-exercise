import boto3

ec2_client = boto3.client('ec2', region_name="ap-southeast-1")

region_subnets = ec2_client.describe_subnets()['Subnets']
for subnet in region_subnets:
  print(f"Subnet: {subnet['SubnetId']}")
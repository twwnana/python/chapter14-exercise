import boto3
import paramiko
import time
import requests
import schedule

ec2_client = boto3.client('ec2', region_name="ap-southeast-1")
ec2_resource = boto3.resource('ec2', region_name="ap-southeast-1")

security_group = ec2_client.create_security_group(
  GroupName="sg_python-exercise14-inbound",
  Description="sg_python-exercise14-inbound",
  VpcId="vpc-0dcc4a26913e1dd41"
)

ec2_resource.SecurityGroup(security_group['GroupId']).authorize_ingress(
  IpPermissions=[{
    'IpProtocol': 'tcp',
    'FromPort': 22,
    'ToPort': 22,
    'IpRanges': [{'CidrIp': '143.44.192.4/32'}]
  }]
)

ec2_resource.SecurityGroup(security_group['GroupId']).authorize_ingress(
  IpPermissions=[{
    'IpProtocol': 'tcp',
    'FromPort': 8080,
    'ToPort': 8080,
    'IpRanges': [{'CidrIp': '143.44.192.4/32'}]
  }]
)

response = ec2_client.run_instances(
  ImageId='ami-06018068a18569ff2',
  InstanceType='t2.micro',
  KeyName="myapp-keypair",
  MaxCount=1,
  MinCount=1,
  NetworkInterfaces=[{
    'AssociatePublicIpAddress': True,
    'DeleteOnTermination': True,
    'DeviceIndex': 0,
    'Groups': [
      security_group['GroupId']
    ],
    'SubnetId': "subnet-01e4ca13d5c74ead2",
  }]
)

new_instance_instanceId = response['Instances'][0]['InstanceId']

while True:
  print("Instance not ready... waiting for 60 seconds ane retrying!.")
  time.sleep(60) # sleep for 60 seconds
  instance_state = ec2_client.describe_instance_status(InstanceIds=[new_instance_instanceId])
  
  if instance_state['InstanceStatuses'][0]['InstanceState']['Name'] == 'running' and \
    instance_state['InstanceStatuses'][0]['InstanceStatus']['Status'] == 'ok' and \
    instance_state['InstanceStatuses'][0]['SystemStatus']['Status'] == 'ok':
    print("Instance is now ready!.")
    break

queried_instance = ec2_client.describe_instances(InstanceIds=[new_instance_instanceId])['Reservations'][0]['Instances'][0]
ssh_client = paramiko.SSHClient()
ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

print(f"connecting to {queried_instance['PublicIpAddress']}.")
ssh_client.connect(hostname=queried_instance['PublicIpAddress'], port=22, username="ec2-user", key_filename="/home/sheenlim08/.ssh/MyKeyPair.pem")

print("Installing docker.")
cmd = "sudo yum update -y && sudo yum install -y docker && sudo systemctl start docker && sudo usermod -aG docker ec2-user"
stdin, stdout, stderr = ssh_client.exec_command(cmd)
print(stdout.readlines())

print("Creating the nginx container.")
cmd = "sudo docker run -d -p 8080:80 --name nginx nginx"
stdin, stdout, stderr = ssh_client.exec_command(cmd)
print(stdout.readlines())

ssh_client.close()

def restartAppContainer():
  ssh_client = paramiko.SSHClient()
  ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
  ssh_client.connect(hostname=queried_instance['PublicIpAddress'], port=22, username="ec2-user", key_filename="/home/sheenlim08/.ssh/MyKeyPair.pem")
  stdin, stdout, stderr = ssh_client.exec_command('docker restart nginx')
  print(stdout.readlines())
  
  ssh_client.close()

def checkAppUpStatus():
  endpoint = f"http://{queried_instance['PublicIpAddress']}:8080"
  try:
    print(f"Testing")
    response = requests.get(endpoint)

    return response.status_code == 200
  except:
    print("Looks like Server is not returning anything.")
    return False
    

def doAppCheck():
  isAppDownCounter = 0
  while isAppDownCounter != 5:
    if (checkAppUpStatus()):
      print("Application is accessible at port 8080 is up.")
    else:
      print("Application is Down.")
      isAppDownCounter += 1
    
    if (isAppDownCounter == 5):
      print("The application is down after 5 checks, restarting application.")
      restartAppContainer()
      isAppDownCounter = 0
    
    time.sleep(5)

schedule.every(120).seconds.do(doAppCheck)

while True:
  schedule.run_pending()
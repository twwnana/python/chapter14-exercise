import boto3
from operator import itemgetter

ec2_client = boto3.client('iam', region_name="ap-southeast-1")

iam_users = ec2_client.list_users()['Users']
aws_users = []
for user in iam_users:
  thisUser = ec2_client.get_user(UserName=user['UserName'])['User']
  
  try:
    # test if the property exist (if a user never logged in this property is not populated.)
    thisUser['PasswordLastUsed']
    
    aws_users.append(thisUser)
    print(f"User {user['UserName']}, last active: {thisUser['PasswordLastUsed']}")
  except:
    next
 
#print(aws_users) 
most_active = sorted(aws_users, key=itemgetter('PasswordLastUsed'), reverse=False)[0]
print(f"Most active user: {most_active['UserName']}")
import boto3
import os

client = boto3.client('ecr', region_name="ap-southeast-1")

repositories = client.describe_repositories(
  registryId=os.environ.get("ECR_REGISTRY_ID"),
  repositoryNames=[
    os.environ.get("ECR_REPO_NAME")
  ]
)

for repo in repositories['repositories']:
  print(f"registryId: {repo['registryId']}")
  print(f"repositoryUri: {repo['repositoryUri']}")
  print(f"repositoryName: {repo['repositoryName']}")
  
user_input = input("Enter the image name you want to list tags: ")

images = client.describe_images(
  registryId=os.environ.get("ECR_REGISTRY_ID"),
  repositoryName=user_input,
)

print("The following are the images tags for the entered respository name.")
for image in images['imageDetails']:
  for tag in image['imageTags']:
    print(tag)